import axios from 'axios';

// set VUE_APP_USE_FLIPSTARTER_DIRECTORY and
// VUE_APP_FLIPSTARTER_DIRECTORY_API variables in .env files
// to use the flipstarter directory instead of a static json file.
// see https://cli.vuejs.org/guide/mode-and-env.html
// see .env.production.example for an example for production deployment
let baseURL = 'http://127.0.0.1:8000';
if (process.env.NODE_ENV === 'production') {
  baseURL = process.env.VUE_APP_FLIPSTARTER_DIRECTORY_API || baseURL;
}

interface Campaign {
  amount: number;
  url: string;
  title: string;
  description: string;
  status: string;
  fundedTx: string;
  categories: Record<string, string>[];
  announcements: Record<string, string>[];
  archives: Record<string, string>[];
}

const getFlipstarters = async () => {
  const response = await axios.get(`${baseURL}/v1/flipstarter/?old`);
  return response.data;
};

const getCategories = async () => {
  const response = await axios.get(`${baseURL}/v1/flipstarter-category/`);
  return response.data;
};

const submitFlipstarter = async (data: Campaign) => {
  const response = await axios.post(`${baseURL}/v1/flipstarter/`, data);
  return response.data;
};

const fetchFlipstarterDataFromUrl = async (url: string) => {
  const response = await axios.post(`${baseURL}/v1/flipstarter-data-from-url/`, { url });
  return response.data;
};

export {
  Campaign,
  fetchFlipstarterDataFromUrl,
  getCategories,
  getFlipstarters,
  submitFlipstarter,
};
